package com.customer.records.server.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import com.customer.records.shared.Customer;




//import com.secure.hib.bean.Address;
//import com.secure.hib.bean.Customer;
//import com.secure.hib.bean.PhoneNumber;
//import com.secure.hib.bean.TimeManager;
//import com.secure.hib.util.HibernateUtils;

public class HibernateTest {

	public static void main(String[] args) 
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
// 
		Customer customer = new Customer();
		customer.setFirstName("V");;
		customer.setLastName("S");;
		customer.setDob(new Date().toLocaleString());
		session.save(customer);
//		List customers = session.createQuery("from Customer").list();
//		System.out.println(customers.get(0));

/*	    TimeManager timeManager = new TimeManager();
	    timeManager.setTime_resynched("21-04-2015 11:20:58");
	    timeManager.setTime_updated("21-04-2015 11:25:34");
	    timeManager.setCustomer(customer);
	    
	    session.save(timeManager);
	    
	    Address address1 = new Address();
	    address1.setAddress("303, S R PRIDE APARTMENT");
	    address1.setCustomer(customer);
	    customer.getAddress().add(address1);
	    Address address2 = new Address();
	    address2.setAddress("NALASHA, HSR LAYOUT");
	    address2.setCustomer(customer);
	    customer.getAddress().add(address2);
	

	    PhoneNumber phoneNumber1 = new PhoneNumber();
	    phoneNumber1.setPhoneNumber("+1-1725698800");
	    phoneNumber1.setCustomer(customer);
	    customer.getPhoneNumber().add(phoneNumber1);
	    PhoneNumber phoneNumber2 = new PhoneNumber();
	    phoneNumber2.setPhoneNumber("+91-9741237861");
	    phoneNumber2.setCustomer(customer);
	    customer.getPhoneNumber().add(phoneNumber2);

	    session.save(address1);
	    session.save(address2);
	    session.save(phoneNumber1);
	    session.save(phoneNumber2);
*/
		session.getTransaction().commit();
 
		System.out.println("Done");

	}

}
