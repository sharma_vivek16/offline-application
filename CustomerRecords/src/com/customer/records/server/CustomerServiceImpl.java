package com.customer.records.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.customer.records.client.app.CustomerService;
import com.customer.records.server.dao.HibernateUtil;
import com.customer.records.shared.Customer;
import com.customer.records.shared.Login;
import com.customer.records.shared.dto.CustomerDTO;
import com.customer.records.shared.util.CustomerRecordsServerDater;
import com.customer.records.shared.util.specification.CustomerRecordsUtil;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */

public class CustomerServiceImpl extends RemoteServiceServlet implements CustomerService 
{

	@Override
	public List<CustomerDTO> getCustomers() throws IllegalArgumentException {
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   Transaction transaction = session.beginTransaction();
	   ArrayList<Customer> customers = new ArrayList<Customer>(session.createQuery("from Customer").list());
	   ArrayList<CustomerDTO> customerDtos = new ArrayList<CustomerDTO>(customers != null ? customers.size() : 0);
	   if (customers != null) 
	   {
		   for (Customer customer : customers) 
		   {
			   customerDtos.add(createCustomerDTO(customer));
	       }
	    }
	    transaction.commit();
	    session.close();
		return customerDtos;
	}
	
	  private CustomerDTO createCustomerDTO(Customer customer)
	  {
		  return new CustomerDTO(customer.getFirstName(), customer.getLastName(), customer.getDob(), customer.getAddress(), customer.getPhoneNumber(), customer.getLastSynchronized());
	  }

	@Override
	public Map<Integer, String> saveCustomer(List<Customer> customers) 
	{
	   CustomerRecordsUtil customerRecordsUtil = new CustomerRecordsServerDater();
	   Map<Integer, String> hashMap = new HashMap<Integer, String>();
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   session.beginTransaction();
	   for(Customer customer : customers)
	   {
		   String date = customerRecordsUtil.getDate();
		   customer.setLastSynchronized(date);
		   hashMap.put(customer.getCustomerId(),date);
		   
		   session.saveOrUpdate(customer);
	   }
	   session.getTransaction().commit();
	   session.close();
	   return hashMap;
	   
	} 
	
	@Override
	public boolean authenticate(String username, String password)
			throws IllegalArgumentException {
		 Session session = HibernateUtil.getSessionFactory().openSession();
		 Transaction transaction = session.beginTransaction();
		 ArrayList<Login> login = new ArrayList<Login>(session.createQuery("from Login").list());
		 transaction.commit();
		 session.close();
		 System.out.println(login.get(0).getUsername().equals(username) +" "+login.get(0).getPassword().equals(password));
		 if (login.get(0).getUsername().equals(username) && login.get(0).getPassword().equals(password))
		 {
			 return true;
		 }
		 return false;
	}
	
	
//	public static void main(String[] args)
//	{
//		CustomerService customerService = new CustomerServiceImpl();
//		customerService.authenticate("nalashaa", "password" );
//	}


}
