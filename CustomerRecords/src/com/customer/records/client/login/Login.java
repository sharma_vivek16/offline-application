package com.customer.records.client.login;

import java.util.logging.Logger;

import org.jboss.logging.Logger.Level;

import com.customer.records.client.app.CustomerService;
import com.customer.records.client.app.CustomerServiceAsync;
import com.customer.records.client.executor.CustomerServiceExecutor;
import com.customer.records.client.executor.specification.Executor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class Login extends Composite implements HasText 
{
	private static LoginUiBinder uiBinder = GWT.create(LoginUiBinder.class);
	interface LoginUiBinder extends UiBinder<Widget, Login>
	{
	}

	public Login()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	   @UiField
	   TextBox loginBox;

	   @UiField
	   PasswordTextBox passwordBox;
	   
	   @UiField
	   Button buttonSubmit;
	   
	   @UiField
	   Label errorLabel;

	public Login(String firstName) 
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("buttonSubmit")
	void onClick(ClickEvent e)
	{
		String username = loginBox.getText();
		String password = passwordBox.getText();
//		Window.alert(username + " "+ password);
		CustomerServiceAsync customerService = GWT.create(CustomerService.class);
		customerService.authenticate(username, password, new AsyncCallback<Boolean>() {
			
			@Override
			public void onSuccess(Boolean result)
			{
				if(result)	
				{				
//					Window.alert("Login Success");		
					RootPanel.get().clear();
				//	LoginCONST.transferFromServerToBrowser = false;
					Executor executor = new CustomerServiceExecutor();
					executor.process();
				}
				else
				{
					errorLabel.setText("Incorrect Username/Password");
				}
			}
			
			@Override
			public void onFailure(Throwable caught)
			{
				RootPanel.get().clear();
				RootPanel.get().add(new Label("Login Failed due to this reason:"+caught.getMessage()));;
				System.err.println(caught.fillInStackTrace());
			}
		});


	}

	@Override
	public String getText() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setText(String text) 
	{
		// TODO Auto-generated method stub
		
	}

}
