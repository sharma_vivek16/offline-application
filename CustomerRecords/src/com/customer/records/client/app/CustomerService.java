package com.customer.records.client.app;

import java.util.List;
import java.util.Map;

import com.customer.records.shared.Customer;
import com.customer.records.shared.dto.CustomerDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("customer")
public interface CustomerService extends RemoteService 
{
	List<CustomerDTO> getCustomers() throws IllegalArgumentException;
	
	Map<Integer, String> saveCustomer(List<Customer> customers);
	
	boolean authenticate(String name, String password) throws IllegalArgumentException;
}
