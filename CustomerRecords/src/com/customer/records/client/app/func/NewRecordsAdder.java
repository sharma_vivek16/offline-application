package com.customer.records.client.app.func;

import com.customer.records.client.app.func.specification.CustomerServiceClientReader;
import com.customer.records.client.executor.BrowserToBrowserOperator;
import com.customer.records.shared.FieldVerifier;
import com.customer.records.shared.util.CustomerRecordsClientDater;
import com.customer.records.shared.util.specification.CustomerRecordsUtil;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class NewRecordsAdder implements CustomerServiceClientReader 
{
    Label errorFirstName;
    Label errorLastName;
    Label errorDob;
    Label errorPhoneNo;
    FieldVerifier verifier = new FieldVerifier();
	CustomerRecordsUtil customerRecordsUtil = new CustomerRecordsClientDater();
	@Override
	public void process(Storage customerRecordsDb, VerticalPanel panel) {
		
		addNewRecords(customerRecordsDb,  panel);
	}
	
	private void addNewRecords(final Storage customerRecordsDb, final VerticalPanel vPanel)
	{
	      Button button = new Button("Add Records", new ClickHandler() 
	      {
			@Override
			public void onClick(ClickEvent event) {
				 HorizontalPanel hPanel = new HorizontalPanel();
				  final TextBox textBox1 = new TextBox(); 
				  textBox1.setFocus(true);
				  final TextBox textBox2 = new TextBox();	
				  final TextBox textBox3 = new TextBox(); 
				  final TextBox textBox4 = new TextBox(); 		
				  final TextBox textBox5 = new TextBox(); 
//				  final TextBox textBox6 = new TextBox(); 
//				  final TextBox textBox7 = new TextBox(); 
			      hPanel.add(textBox1);				      
			      hPanel.add(textBox2);
			      hPanel.add(textBox3);
			      hPanel.add(textBox4);
			      hPanel.add(textBox5);
//			      hPanel.add(textBox6);
//			      hPanel.add(textBox7);
			      vPanel.add(hPanel);

			      final int j = Integer.parseInt(customerRecordsDb.getItem(CustomerServiceCONST.COUNT));
			     // int rowsInStorage = Integer.parseInt(customerRecordsDb.getItem(COUNT));
				  customerRecordsDb.setItem(CustomerServiceCONST.COUNT, String.valueOf(j + 1));
//				  Window.alert("COUNT updated to "+String.valueOf(Integer.parseInt(customerRecordsDb.getItem(COUNT) )+ 1));
				  
			        textBox1.addBlurHandler(new BlurHandler() {				
						  @Override
						  public void onBlur(BlurEvent event) {
							  if(verifier.isValidNameForNewRecord(textBox1.getText()))
							  {
								  customerRecordsDb.setItem(CustomerServiceCONST.FIRST_NAME + j, textBox1.getText());
								  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED+j, customerRecordsUtil.getDate());
								  if(errorFirstName != null)
								  {
									  vPanel.remove(errorFirstName);
								  }

							  }
							  else
							  {
								   if(errorFirstName == null) errorFirstName = new Label();
					    		   errorFirstName.setStyleName("serverResponseLabelError");
								   errorFirstName.setText("First Name can only have alphabets. This value will not be taken.");
								   vPanel.add(errorFirstName);
							  }
						  }

						  });
						  				  
					      textBox2.addBlurHandler(new BlurHandler() {				
						  @Override
						  public void onBlur(BlurEvent event) {
							  if(verifier.isValidNameForNewRecord(textBox2.getText()))
							  {							  
								  customerRecordsDb.setItem(CustomerServiceCONST.LAST_NAME + j, textBox2.getText());
	//							  Window.alert("Retrieve LastName: "+customerRecordsDb.getItem(LAST_NAME + j)+ "@ "+LAST_NAME + j);
								  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED+j, customerRecordsUtil.getDate());
								  if(errorLastName != null)
								  {
									  vPanel.remove(errorLastName);
								  }
							  }
							  else
							  {
								  if (errorLastName == null) errorLastName = new Label();
								  errorLastName.setStyleName("serverResponseLabelError");
								  errorLastName.setText("Last Name can only have alphabets. This value will not be taken.");
								  vPanel.add(errorLastName);
							  }
							}
						  });
					     
						  textBox3.addBlurHandler(new BlurHandler() {				
						  @Override
						  public void onBlur(BlurEvent event) {
							  if(verifier.isValidDateForNewRecord(textBox3.getText()))
							  {							  
								  customerRecordsDb.setItem(CustomerServiceCONST.DATE_OF_BIRTH + j, textBox3.getText());
	//							  Window.alert("Retrieve DOB: "+customerRecordsDb.getItem(DATE_OF_BIRTH + j) + "@ "+DATE_OF_BIRTH + j);
								  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED+j, customerRecordsUtil.getDate());
								  if(errorDob != null)
								  {
									  vPanel.remove(errorDob);
								  }							
							  }
							  else
							  {
								  if (errorDob == null) errorDob = new Label();
								  errorDob.setStyleName("serverResponseLabelError");
								  errorDob.setText("Date of Birth should have the format dd-mm-yyyy. This value will not be taken.");
								  vPanel.add(errorDob);
					  
							  }

							  
							}
						  });
						  
						  textBox4.addBlurHandler(new BlurHandler() {				
						  @Override
						  public void onBlur(BlurEvent event) {
							  if(textBox4.getText() != null && !textBox4.getText().isEmpty())
							  {							  
								  customerRecordsDb.setItem(CustomerServiceCONST.ADDRESS + j, textBox4.getText());
	//							  Window.alert("Retrieve DOB: "+customerRecordsDb.getItem(DATE_OF_BIRTH + j) + "@ "+DATE_OF_BIRTH + j);
								  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED+j, customerRecordsUtil.getDate());
							  }
							}
						  });
						  
						  textBox5.addBlurHandler(new BlurHandler() {				
						  @Override
						  public void onBlur(BlurEvent event) {
							  if(verifier.isValidPhoneNumberForNewRecord(textBox5.getText()))
							  {
								  customerRecordsDb.setItem(CustomerServiceCONST.PHONE_NUMBER + j, textBox5.getText());
	//							  Window.alert("Retrieve DOB: "+customerRecordsDb.getItem(DATE_OF_BIRTH + j) + "@ "+DATE_OF_BIRTH + j);
								  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED+j, customerRecordsUtil.getDate());
								  if(errorPhoneNo != null)
								  {
									  vPanel.remove(errorPhoneNo);
								  }						  
							  }
							  else
							  {
								  if (errorPhoneNo == null) errorPhoneNo = new Label();
								  errorPhoneNo.setStyleName("serverResponseLabelError");
								  errorPhoneNo.setText("Phone Number can only be of 12 digits. This value will not be taken.");
								  vPanel.add(errorPhoneNo);
								  
							  }

							}
						  });
						  
				  
				}
			
			
			
				});
	      
			  final BrowserToBrowserOperator reader = new BrowserToBrowserOperator();
		      Button button1 = new Button("Refresh Records", new ClickHandler() 
		      {
				@Override
				public void onClick(ClickEvent event) {
					reader.showRecords(customerRecordsDb);
				}
		      });
		      HorizontalPanel hpnl = new HorizontalPanel();
		      hpnl.add(button);
	          hpnl.add(button1);

	          vPanel.add(hpnl);

			
		}
	

	
	 
}
