package com.customer.records.client.app.func.specification;

import com.customer.records.client.app.CustomerServiceAsync;
import com.google.gwt.storage.client.Storage;

public interface CustomerServiceClientWriter extends ServiceImplementor<Storage,CustomerServiceAsync>
{
	public void process(Storage customerRecordsDb, CustomerServiceAsync customerService) ;
}
