package com.customer.records.client.app.func.specification;

import com.google.gwt.user.client.ui.VerticalPanel;

public interface CustomerServiceInternetStatus 
{
	public void process(VerticalPanel vPanel);
}
