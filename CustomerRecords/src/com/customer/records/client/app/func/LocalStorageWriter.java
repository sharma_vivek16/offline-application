package com.customer.records.client.app.func;

import java.util.List;

import com.customer.records.client.app.CustomerServiceAsync;
import com.customer.records.client.app.func.specification.CustomerServiceClientWriter;
import com.customer.records.client.executor.BrowserToBrowserOperator;
import com.customer.records.client.executor.BrowserToServerOperator;
import com.customer.records.shared.dto.CustomerDTO;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LocalStorageWriter implements CustomerServiceClientWriter 
{

	@Override
	public void process(Storage customerRecordsDb, CustomerServiceAsync customerService) 
	{		
//		Window.alert("writing started");
		getCustomerRecordsFromServerDb(customerService, customerRecordsDb);
//		Window.alert("writing Finished");
	}
	
    private void writeIntoLocalStorage(Storage customerRecordsDb, List<CustomerDTO> result)
    {
		if (customerRecordsDb != null) 
	    {     
	    	  int rows=0;
	    	  for (CustomerDTO customer : result)
	    	  {		
	    		  String row = String.valueOf(rows);
//	    		  Window.alert(CustomerServiceCONST.FIRST_NAME+row+":"+customer.getFirstName() + " "
//	    				  + CustomerServiceCONST.LAST_NAME+row+":"+customer.getLastName() + " " +CustomerServiceCONST.DATE_OF_BIRTH+row+":"+customer.getDob().toString());
	    		  customerRecordsDb.setItem(CustomerServiceCONST.FIRST_NAME+row, customer.getFirstName());
	    		  customerRecordsDb.setItem(CustomerServiceCONST.LAST_NAME+row, customer.getLastName());		    		  
	    		  customerRecordsDb.setItem(CustomerServiceCONST.DATE_OF_BIRTH+row,  customer.getDob().toString());
	    		  customerRecordsDb.setItem(CustomerServiceCONST.ADDRESS+row, customer.getAddress());
	    		  customerRecordsDb.setItem(CustomerServiceCONST.PHONE_NUMBER+row, customer.getPhoneNumber());
	    		  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED+row, customer.getLastSynchronized());
	       		  customerRecordsDb.setItem(CustomerServiceCONST.LAST_SYNCHRONIZED+row, customer.getLastSynchronized()); 		
	    		  rows++;
	    	  }
	    	  customerRecordsDb.setItem(CustomerServiceCONST.COUNT, String.valueOf(String.valueOf(rows))); 
	    }
    }
    
	private void getCustomerRecordsFromServerDb(final CustomerServiceAsync customerService, final Storage customerRecordsDb)	
	{	
		customerService.getCustomers(new AsyncCallback<List<CustomerDTO>>() 
		{
			@Override
			public void onFailure(Throwable caught) 
			{
				Window.alert("Failed Miserably");				
			}
	
			@Override
			public void onSuccess(List<CustomerDTO> result)
			{
//				Window.alert("Success");
				writeIntoLocalStorage(customerRecordsDb, result);
				//Read the data from the browser cache
				
//				Window.alert("Can Start Other Operations");
				BrowserToBrowserOperator b2bOperator = new BrowserToBrowserOperator();
				VerticalPanel vPanel = b2bOperator.showRecords(customerRecordsDb);
				
				//Synchronize data from browser cache to the server db periodically
				BrowserToServerOperator b2sOperator = new BrowserToServerOperator();
				b2sOperator.syncDataToTheServer(customerRecordsDb, customerService);

			}
	    });
	}

}
