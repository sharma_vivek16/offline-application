package com.customer.records.client.app.func;

import org.apache.tools.ant.types.resources.First;

import com.customer.records.client.app.func.specification.CustomerServiceClientReader;
import com.customer.records.shared.FieldVerifier;
import com.customer.records.shared.util.CustomerRecordsClientDater;
import com.customer.records.shared.util.specification.CustomerRecordsUtil;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LocalStorageReader  implements CustomerServiceClientReader
{
	CustomerRecordsUtil customerRecordsUtil = new CustomerRecordsClientDater();
    FieldVerifier verifier = new FieldVerifier();
    Label errorFirstName;
    Label errorLastName;
    Label errorDob;
    Label errorPhoneNo;
    @Override
	public void process(Storage customerRecordsDb, VerticalPanel panel) 
	{	
//		Window.alert("reading started");
		readFromLocalStorage(customerRecordsDb,  panel);
//		Window.alert("reading Finished");

	}
	
    private void readFromLocalStorage(final Storage customerRecordsDb, final VerticalPanel vPanel)
    {
//		Window.alert("Reading:"+customerRecordsDb);

		if (customerRecordsDb != null) 
	    {   

    		  final FlexTable flexTable = new FlexTable();
    		  flexTable.setHTML(0, 0, "First Name");
    		  flexTable.setHTML(0, 1, "Last Name");
    		  flexTable.setHTML(0, 2, "Date of Birth");
    		  flexTable.setHTML(0, 3, "Address");
    		  flexTable.setHTML(0, 4, "Phone Number");
    		  flexTable.setHTML(0, 5, "Last Updated");
    		  flexTable.setHTML(0, 6, "Last Synchronized");    		  
    		  flexTable.setBorderWidth(2);
    	
    		  final int rows = Integer.parseInt(customerRecordsDb.getItem(CustomerServiceCONST.COUNT));
    		  

			  
	    	  for (int i=1; i<=rows; i++)
	    	  {
				  String row = String.valueOf(i-1);
				  int count=0;


//				  Window.alert(FIRST_NAME+row+":"+customerRecordsDb.getItem(FIRST_NAME+row) + " "+
//				  LAST_NAME+row+":"+customerRecordsDb.getItem(LAST_NAME+row)+" "+
//				  DATE_OF_BIRTH+":"+customerRecordsDb.getItem(DATE_OF_BIRTH+row));
				if (customerRecordsDb.getItem(CustomerServiceCONST.FIRST_NAME + row) != null ||
						customerRecordsDb.getItem(CustomerServiceCONST.LAST_NAME + row) != null ||
						customerRecordsDb.getItem(CustomerServiceCONST.DATE_OF_BIRTH + row) != null ||
						customerRecordsDb.getItem(CustomerServiceCONST.ADDRESS + row) != null ||
						customerRecordsDb.getItem(CustomerServiceCONST.PHONE_NUMBER + row) != null ||
						customerRecordsDb.getItem(CustomerServiceCONST.LAST_UPDATED + row) != null ||
						customerRecordsDb.getItem(CustomerServiceCONST.LAST_SYNCHRONIZED + row)!=null)
				  {
				  
				  final TextBox textBox1 = new TextBox();
				  String text1 = customerRecordsDb.getItem(CustomerServiceCONST.FIRST_NAME+row);
//				  Window.alert(text1);
				  if (text1!=null && !text1.isEmpty())
				  {
					  textBox1.setText(text1);
				  }
				  textBox1.setName(CustomerServiceCONST.FIRST_NAME+row);					 
				  flexTable.setWidget(i, count++, textBox1);
				  textBox1.setReadOnly(true);
				  
				  final TextBox textBox2 = new TextBox();
				  String text2 = customerRecordsDb.getItem(CustomerServiceCONST.LAST_NAME+row);
				  if (text2!=null && !text2.isEmpty())
				  {
					  textBox2.setText(text2);
				  }
				  textBox2.setName(CustomerServiceCONST.LAST_NAME+row);					 
	    		  flexTable.setWidget(i, count++, textBox2);
	    		  textBox2.setReadOnly(true);
				  
	    		  final TextBox textBox3 = new TextBox();
	    		  String text3 = customerRecordsDb.getItem(CustomerServiceCONST.DATE_OF_BIRTH+row);
				  if (text3!=null && !text3.isEmpty())
				  {
					  textBox3.setText(text3);
				  }
				  textBox3.setName(CustomerServiceCONST.DATE_OF_BIRTH+row);						
	    		  flexTable.setWidget(i, count++, textBox3);
	    		  textBox3.setReadOnly(true);
	    		  
	    		  final TextBox textBox4 = new TextBox();
	    		  String text4 = customerRecordsDb.getItem(CustomerServiceCONST.ADDRESS+row);
				  if (text4!=null && !text4.isEmpty())
				  {
					  textBox4.setText(text4);
				  }
				  textBox4.setName(CustomerServiceCONST.ADDRESS+row);				
	    		  flexTable.setWidget(i, count++, textBox4);
	    		  textBox4.setReadOnly(true);
	    		  
	    		  final TextBox textBox5 = new TextBox();
	    		  String text5 = customerRecordsDb.getItem(CustomerServiceCONST.PHONE_NUMBER+row);
				  if (text5!=null && !text5.isEmpty())
				  {
					  textBox5.setText(text5);
				  }
				  textBox5.setName(CustomerServiceCONST.PHONE_NUMBER+row);						  
	    		  flexTable.setWidget(i, count++, textBox5);
	    		  textBox5.setReadOnly(true);
	    		  
	    		  TextBox textBox6 = new TextBox();
	    		  textBox6.setText(customerRecordsDb.getItem(CustomerServiceCONST.LAST_UPDATED+row));
	    		  flexTable.setWidget(i, count++, textBox6);
	    		  textBox6.setReadOnly(true);
	    		  
	    		  TextBox textBox7 = new TextBox();
	    		  textBox7.setText(customerRecordsDb.getItem(CustomerServiceCONST.LAST_SYNCHRONIZED+row));
	    		  flexTable.setWidget(i, count++, textBox7);
	    		  textBox7.setReadOnly(true);
				  

				  
				  Button button1 = new Button("Edit Records", new ClickHandler() {
						
						@Override
						public void onClick(ClickEvent event) {
					
							textBox1.setReadOnly(false);
							textBox2.setReadOnly(false);
							textBox3.setReadOnly(false);
							textBox4.setReadOnly(false);
							textBox5.setReadOnly(false);

							
						}
					});
				  flexTable.setWidget(i, count++, button1);


				 
			      textBox1.addBlurHandler(new BlurHandler() {				
				  @Override
				  public void onBlur(BlurEvent event) {
					  
					  String key = textBox1.getName();
					  String value = textBox1.getText();
					  if(verifier.isValidName(value))
					  {
						  customerRecordsDb.setItem(key, value);
	//					  Window.alert("Retrieve FirstName: "+customerRecordsDb.getItem(key) + "@ "+FIRST_NAME + key);
						  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED + key.substring(3,key.length()), customerRecordsUtil.getDate());
//						  Window.alert(String.valueOf(checkErrorEntries(rows, 0, flexTable, "FIRSTNAME")));
						  if(errorFirstName != null && !verifier.checkErrorEntries(rows, 0, flexTable, "FIRSTNAME"))
						  {
							  vPanel.remove(errorFirstName);
						  }

					  }
					  else
					  {
						   if(errorFirstName == null) errorFirstName = new Label();
			    		   errorFirstName.setStyleName("serverResponseLabelError");
						   errorFirstName.setText("First Name can only have alphabets. This value will not be taken.");
						   vPanel.add(errorFirstName);

					  }
					}
				  });
			      textBox2.addBlurHandler(new BlurHandler() {				
				  @Override
				  public void onBlur(BlurEvent event) {
					  
					  String key = textBox2.getName();
					  String value = textBox2.getText();
					  if(verifier.isValidName(value))
					  {
						  customerRecordsDb.setItem(key, value);
	//					  Window.alert("Retrieve FirstName: "+customerRecordsDb.getItem(key) + "@ "+FIRST_NAME + key);
						  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED + key.substring(3,key.length()), customerRecordsUtil.getDate());
//						  Window.alert(String.valueOf(checkErrorEntries(rows, 1, flexTable, "LASTNAME")));
						  if(errorLastName != null && !verifier.checkErrorEntries(rows, 1, flexTable, "LASTNAME"))
						  {
							  vPanel.remove(errorLastName);
						  }
					  }
					  else
					  {
						  if (errorLastName == null) errorLastName = new Label();
						  errorLastName.setStyleName("serverResponseLabelError");
						  errorLastName.setText("Last Name can only have alphabets. This value will not be taken.");
						  vPanel.add(errorLastName);
					  }
					}
				  });
			      textBox3.addBlurHandler(new BlurHandler() {				
				  @Override
				  public void onBlur(BlurEvent event) {
					  
					  String key = textBox3.getName();
					  String value = textBox3.getText();
					  if(verifier.isValidDate(value))
					  {
						  customerRecordsDb.setItem(key, value);
//						  Window.alert("Retrieve FirstName: "+customerRecordsDb.getItem(key) + "@ "+FIRST_NAME + key);
						  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED + key.substring(3,key.length()), customerRecordsUtil.getDate());
						  if(errorDob != null && !verifier.checkErrorEntries(rows, 2, flexTable, "DATEOFBIRTH"))
						  {
							  vPanel.remove(errorDob);
						  }							
					  }
					  else
					  {
						  if (errorDob == null) errorDob = new Label();
						  errorDob.setStyleName("serverResponseLabelError");
						  errorDob.setText("Date of Birth should have the format dd-mm-yyyy. This value will not be taken.");
						  vPanel.add(errorDob);
					  }
					}
				  });
			      textBox4.addBlurHandler(new BlurHandler() {				
				  @Override
				  public void onBlur(BlurEvent event) {
					  
					  String key = textBox4.getName();
					  String value = textBox4.getText();

					  customerRecordsDb.setItem(key, value);
//					  Window.alert("Retrieve FirstName: "+customerRecordsDb.getItem(key) + "@ "+FIRST_NAME + key);
					  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED + key.substring(3,key.length()), customerRecordsUtil.getDate());	
				 
					}
				  });
			      
			      textBox5.addBlurHandler(new BlurHandler() {				
				  @Override
				  public void onBlur(BlurEvent event) {
					  
					  String key = textBox5.getName();
					  String value = textBox5.getText();
					  if(verifier.isValidPhoneNumber(value))
					  {
						  customerRecordsDb.setItem(key, value);
	//					  Window.alert("Retrieve FirstName: "+customerRecordsDb.getItem(key) + "@ "+FIRST_NAME + key);
						  customerRecordsDb.setItem(CustomerServiceCONST.LAST_UPDATED + key.substring(3,key.length()), customerRecordsUtil.getDate());	
						  if(errorPhoneNo != null && !verifier.checkErrorEntries(rows, 2, flexTable, "PHONENUMBER"))
						  {
							  vPanel.remove(errorPhoneNo);
						  }						  
					  }
					  else
					  {
						  if (errorPhoneNo == null) errorPhoneNo = new Label();
						  errorPhoneNo.setStyleName("serverResponseLabelError");
						  errorPhoneNo.setText("Phone Number can only be of 12 digits. This value will not be taken.");
						  vPanel.add(errorPhoneNo);
					  }
					}
				  });
	    	  }
	    	  }
	    	 
	  		  HorizontalPanel hpanel = new HorizontalPanel();


	  		  hpanel.add(flexTable);
	  		  vPanel.add(hpanel);
		      // Add the widgets to the root panel.
	    }	

    }
    

    
}
