package com.customer.records.client.app.func;

public class CustomerServiceCONST
{
	public static final String FIRST_NAME = "fnm";
	public static final String DATE_OF_BIRTH = "dob";
	public static final String LAST_NAME = "lnm";
	public static final String ADDRESS = "add";
	public static final String COUNT = "count";
	public static final String PHONE_NUMBER = "phn";
	public static final String LAST_UPDATED = "lup";
	public static final String LAST_SYNCHRONIZED = "lsy";
}
