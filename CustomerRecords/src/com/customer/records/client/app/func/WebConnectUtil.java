package com.customer.records.client.app.func;


import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;


;
public class WebConnectUtil extends Label{
	
	public boolean flag;
	private static final String ONLINE="Online";
	private static final String OFFLINE="Offline";
	private static final int INTERVAL=5000;
	private Timer timer;

	public WebConnectUtil(){
		//setSize(16);
		timer=new Timer() {
			@Override
			public void run() {
				checkConnectivity(WebConnectUtil.this);
			}
		};
		addAttachHandler(new Handler() {
			
			@Override
			public void onAttachOrDetach(AttachEvent event) {
				timer.scheduleRepeating(INTERVAL);
				
			}
		});	
	}
	private void online(){
	//	Window.alert("online detected");
		this.setText(ONLINE);

	}
	private void offline(){
	//	Window.alert("offline detected");
		this.setText(OFFLINE);

	}
	
	public native boolean checkConnectivity(WebConnectUtil checker) /*-{
    var img = new Image();
    img.onload = function() { 
    //	window.alert("online");          
      checker.@com.customer.records.client.app.func.WebConnectUtil::online()();
    }
    img.onerror = function() {  
    	//   window.alert("offline");                
      checker.@com.customer.records.client.app.func.WebConnectUtil::offline()();
    }
    img.src = "http://cdn.sstatic.net/stackoverflow/img/apple-touch-icon.png?v=41f6e13ade69&state="+Math.floor(Math.random() * 100);
}-*/;
}
