package com.customer.records.client.app.func;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.customer.records.client.app.CustomerServiceAsync;
import com.customer.records.client.app.func.specification.CustomerServiceClientWriter;
import com.customer.records.shared.Customer;
import com.customer.records.shared.util.CustomerRecordsClientDater;
import com.customer.records.shared.util.specification.CustomerRecordsUtil;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public class ServerDbSynchronizer implements CustomerServiceClientWriter
{
	public void process(final Storage customerRecordsDb, final CustomerServiceAsync customerService)
	{
//		Window.alert("synching Started");
		syncToServerDb(customerRecordsDb,  customerService);
//		Window.alert("synching Finished");

	}
	
	
	

	
	private void syncToServerDb(final Storage customerRecordsDb, final CustomerServiceAsync customerService)
	{
//		  Window.alert("syncToServerDb");
		  final CustomerRecordsUtil customerRecordsUtil = new CustomerRecordsClientDater();

//		  {
//	        @Override
//	        public void run()
//		    while(true)

//	        {	
	        	List<Customer> customerRecordsList = new ArrayList<Customer>();
	        	int rows = Integer.parseInt(customerRecordsDb.getItem(CustomerServiceCONST.COUNT));
//	        	Window.alert("Repeat:"+rows);
	        	for (int i=0; i<rows; i++)
	        	{
	        		String lastUpdated = customerRecordsDb.getItem(CustomerServiceCONST.LAST_UPDATED + i);		
        			String lastSynched = customerRecordsDb.getItem(CustomerServiceCONST.LAST_SYNCHRONIZED + i);
//	        		Window.alert("lastUpdated:"+lastUpdated+"lastSynched:"+lastSynched);
	        		if (doesCustomerExists(customerRecordsDb,  i) && (null == lastSynched || customerRecordsUtil.compareDates(lastUpdated, lastSynched)))
	        		{
	        			Customer customer = new Customer(i,
	        					customerRecordsDb.getItem(CustomerServiceCONST.FIRST_NAME + i),
	        					customerRecordsDb.getItem(CustomerServiceCONST.LAST_NAME + i),
	        					customerRecordsDb.getItem(CustomerServiceCONST.DATE_OF_BIRTH + i),
	        					customerRecordsDb.getItem(CustomerServiceCONST.ADDRESS + i),
	        					customerRecordsDb.getItem(CustomerServiceCONST.PHONE_NUMBER + i)); 
	        			customerRecordsList.add(customer);
	        		}
	        	}
//	        	Window.alert("TrackerSize:"+customerRecordsList.size());
	        	if(customerRecordsList.size()>0)
	        	{
		        	customerService.saveCustomer(customerRecordsList, new AsyncCallback<Map<Integer, String>>()
		        	{
	
		    			@Override
		    			public void onFailure(Throwable caught) 
		    			{
		    				System.err.println("Could not synch the data:"+caught.fillInStackTrace());			
		    			}
	
						@Override
						public void onSuccess(Map<Integer, String> result) {
								for(Map.Entry<Integer, String> entry : result.entrySet())
								{
//									Window.alert("Save Successful");
									customerRecordsDb.setItem(CustomerServiceCONST.LAST_SYNCHRONIZED + entry.getKey().intValue(), entry.getValue());
//									Window.alert("Retrieve " + customerRecordsDb.getItem(CustomerServiceCONST.LAST_SYNCHRONIZED + entry.getKey().intValue()));
								}
						}
		        	});
	        	}
	        	
	        }
	
			private boolean doesCustomerExists( Storage customerRecordsDb, int i)
			{
				boolean exists = customerRecordsDb.getItem(CustomerServiceCONST.FIRST_NAME + i) != null ||
		    					 customerRecordsDb.getItem(CustomerServiceCONST.FIRST_NAME + i) != null ||
		    					 customerRecordsDb.getItem(CustomerServiceCONST.LAST_NAME + i) != null ||
		    					 customerRecordsDb.getItem(CustomerServiceCONST.DATE_OF_BIRTH + i) != null ||
		    					 customerRecordsDb.getItem(CustomerServiceCONST.ADDRESS + i) != null ||
		    					 customerRecordsDb.getItem(CustomerServiceCONST.PHONE_NUMBER + i) != null ||
		    					 customerRecordsDb.getItem(CustomerServiceCONST.LAST_SYNCHRONIZED + i) != null;
				return exists;
			}
//		  };
//		  refreshTimer.schedule(30000);
//		  refreshTimer.scheduleRepeating(30000);;
//	}
}
