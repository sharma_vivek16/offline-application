package com.customer.records.client.app.func.specification;

public interface ServiceImplementor<T, U>
{	
	 public void process(T t, U u);
}
