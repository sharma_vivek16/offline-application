package com.customer.records.client.app.func.specification;

import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.ui.VerticalPanel;

public interface CustomerServiceClientReader extends ServiceImplementor<Storage, VerticalPanel>
{
	public void process(Storage customerRecordsDb, VerticalPanel panel);
}
