package com.customer.records.client.app.func;

import com.customer.records.client.app.func.specification.CustomerServiceInternetStatus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class InternetConnectivityAlarmer implements CustomerServiceInternetStatus
{
	@Override
	public void process(VerticalPanel vPanel) 
	{
		internetConnectionListener(vPanel);
	}
	
	private void internetConnectionListener(final VerticalPanel vPanel)
	{
//		 WebConnectUtil webConnect = new WebConnectUtil();
//		 vPanel.add(webConnect);
		 final Label label = new Label();
		 vPanel.add(label);
		 RootPanel.get().add(vPanel);
			Timer timer=new Timer() {
				@Override
				public void run() {
					if(isOnline())
					{
						label.setText("ONLINE");
					}
					else
					{
						label.setText("OFFLINE");
					}
				}
			};timer.scheduleRepeating(5000);
	
		} 

	public native boolean isOnline() /*-{
	return $wnd.navigator.onLine;
	}-*/;


}
