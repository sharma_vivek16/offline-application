package com.customer.records.client.app;

import java.util.List;
import java.util.Map;

import com.customer.records.shared.Customer;
import com.customer.records.shared.dto.CustomerDTO;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface CustomerServiceAsync 
{
	public void getCustomers(AsyncCallback<List<CustomerDTO>> callback)
			throws IllegalArgumentException;

	void saveCustomer(List<Customer> customers,
			AsyncCallback<Map<Integer, String>> asyncCallback);

	void authenticate(String name, String passw, AsyncCallback<Boolean> callback);
}
