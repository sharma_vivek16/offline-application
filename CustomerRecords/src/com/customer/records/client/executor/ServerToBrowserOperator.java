package com.customer.records.client.executor;

import com.customer.records.client.app.CustomerServiceAsync;
import com.customer.records.client.app.func.LocalStorageWriter;
import com.customer.records.client.app.func.specification.CustomerServiceClientWriter;
import com.customer.records.client.executor.specification.Executor;
import com.customer.records.client.executor.specification.Operator;
import com.customer.records.client.login.LoginCONST;
import com.google.gwt.storage.client.Storage;

public class ServerToBrowserOperator implements Operator {
	public void loadRecordsFromServerToBrowserCache(CustomerServiceAsync customerService, Storage customerRecordsDb)
	{
			CustomerServiceClientWriter writer = new LocalStorageWriter();
			writer.process(customerRecordsDb, customerService);	

	
	}
}
