package com.customer.records.client.executor.specification;

public interface Executor 
{
	public void process();
}
