package com.customer.records.client.executor;

import com.customer.records.client.app.func.InternetConnectivityAlarmer;
import com.customer.records.client.app.func.LocalStorageReader;
import com.customer.records.client.app.func.NewRecordsAdder;
import com.customer.records.client.app.func.specification.CustomerServiceClientReader;
import com.customer.records.client.app.func.specification.CustomerServiceInternetStatus;
import com.customer.records.client.executor.specification.Operator;
import com.customer.records.client.login.LoginCONST;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class BrowserToBrowserOperator implements Operator {
	
	private void readDataFromBrowserCacheToPage(Storage customerRecordsDb, VerticalPanel vPanel)
	{
		CustomerServiceClientReader reader = new LocalStorageReader();
		reader.process(customerRecordsDb, vPanel);			
	}
	
	private void addAddRecordButton(Storage customerRecordsDb, VerticalPanel vPanel)
	{
		CustomerServiceClientReader reader =  new NewRecordsAdder();
		reader.process(customerRecordsDb, vPanel);
	}
	
	private void showInternetStatus(VerticalPanel vPanel)
	{
		CustomerServiceInternetStatus status = new InternetConnectivityAlarmer();
		status.process(vPanel);
	}
	
	public VerticalPanel showRecords(Storage customerRecordsDb)
	{		
		RootPanel.get().clear();
		DecoratorPanel decoratorPanel = new DecoratorPanel();
		VerticalPanel vPanel = new VerticalPanel();
		decoratorPanel.add(vPanel);
	    RootPanel.get().add(decoratorPanel);
	    showInternetStatus(vPanel);
	

		readDataFromBrowserCacheToPage(customerRecordsDb, vPanel);
		addAddRecordButton(customerRecordsDb, vPanel);		
	
		return vPanel;
	}
}
