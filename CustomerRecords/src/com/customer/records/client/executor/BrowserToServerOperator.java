package com.customer.records.client.executor;

import com.customer.records.client.app.CustomerServiceAsync;
import com.customer.records.client.app.func.ServerDbSynchronizer;
import com.customer.records.client.app.func.specification.CustomerServiceClientWriter;
import com.customer.records.client.executor.specification.Operator;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Timer;

public class BrowserToServerOperator implements Operator{

	public void syncDataToTheServer(final Storage customerRecordsDb, final CustomerServiceAsync customerService)
	{							
		final CustomerServiceClientWriter synchronizer = new ServerDbSynchronizer();
		Timer timer = new Timer(){
			@Override
			public void run() {
				synchronizer.process(customerRecordsDb, customerService);
			}
			
		};timer.scheduleRepeating(60000);


	}
}
