package com.customer.records.client.executor;

import com.customer.records.client.app.CustomerService;
import com.customer.records.client.app.CustomerServiceAsync;
import com.customer.records.client.executor.specification.Executor;
import com.customer.records.client.login.LoginCONST;
import com.google.gwt.core.client.GWT;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.ui.VerticalPanel;

public class CustomerServiceExecutor implements Executor
{
	//public static boolean writeOnly = true;
	@Override
	public void process() 
	{		
		CustomerServiceAsync customerService = GWT.create(CustomerService.class);
		Storage customerRecordsDb = Storage.getLocalStorageIfSupported();
		
		//Write the data from the server db to browser cache immediately after login.
		ServerToBrowserOperator s2bOperator =  new ServerToBrowserOperator();
		s2bOperator.loadRecordsFromServerToBrowserCache(customerService, customerRecordsDb);
		

	}
	
		

}
	
	 

