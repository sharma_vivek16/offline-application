package com.customer.records.shared.util;

import java.util.Date;

import com.customer.records.shared.util.specification.CustomerRecordsUtil;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.Window;

public class CustomerRecordsClientDater implements CustomerRecordsUtil
{

	public String getDate()
	{
		Date date = new Date();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
		String strDate = dateFormat.format(date);
		return strDate;
	}
	
	public boolean compareDates(String lastUpdated, String lastSynched)
	{
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
		Date uDate = dateFormat.parse(lastUpdated);
		Date sDate = dateFormat.parse(lastSynched);
		if(uDate.after(sDate))
		{
			return true;
		}
		return false;
	}
	
}
