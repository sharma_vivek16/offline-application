package com.customer.records.shared.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.customer.records.shared.util.specification.CustomerRecordsUtil;

public class CustomerRecordsServerDater implements CustomerRecordsUtil {

	@Override
	public String getDate() {
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String strDate = sdf.format(today);
		return strDate;
	}

	@Override
	public boolean compareDates(String lastUpdated, String lastSynched) {		
		return false;
	}
	
}
