package com.customer.records.shared.util.specification;

public interface CustomerRecordsUtil {
	String getDate();
	boolean compareDates(String lastUpdated, String lastSynched);
}
