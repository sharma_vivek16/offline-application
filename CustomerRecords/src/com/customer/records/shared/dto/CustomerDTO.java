package com.customer.records.shared.dto;

import java.io.Serializable;

public class CustomerDTO implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String dob;
	private String address;
	private String phoneNumber;
	
	private String lastSynchronized;
	
	public CustomerDTO() 
	{

	}
	
	public CustomerDTO(String firstName, String lastName, String dob, String address, String phoneNumber,String lastSynchronized )
    {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = dob;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.lastSynchronized = lastSynchronized;
	}
	
	public String getAddress() 
	{
		return address;
	}

	public String getDob() 
	{
		return dob;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName() 
	{
		return lastName;
	}

	public String getLastSynchronized() 
	{
		return lastSynchronized;
	}

	public String getPhoneNumber() 
	{
		return phoneNumber;
	}

	public void setAddress(String address) 
	{
		this.address = address;
	}

	public void setDob(String dob) 
	{
		this.dob = dob;
	}

	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}

	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}

	public void setLastSynchronized(String lastSynchronized) 
	{
		this.lastSynchronized = lastSynchronized;
	}

	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}
}
