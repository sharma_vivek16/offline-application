package com.customer.records.shared;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;

/**
 * <p>
 * FieldVerifier validates that the name the user enters is valid.
 * </p>
 * <p>
 * This class is in the <code>shared</code> package because we use it in both
 * the client code and on the server. On the client, we verify that the name is
 * valid before sending an RPC request so the user doesn't have to wait for a
 * network round trip to get feedback. On the server, we verify that the name is
 * correct to ensure that the input is correct regardless of where the RPC
 * originates.
 * </p>
 * <p>
 * When creating a class that is used on both the client and the server, be sure
 * that all code is translatable and does not use native JavaScript. Code that
 * is not translatable (such as code that interacts with a database or the file
 * system) cannot be compiled into client-side JavaScript. Code that uses native
 * JavaScript (such as Widgets) cannot be run on the server.
 * </p>
 */
public class FieldVerifier {

	/**
	 * Verifies that the specified name is valid for our service.
	 * 
	 * In this example, we only require that the name is at least four
	 * characters. In your application, you can use more complex checks to ensure
	 * that usernames, passwords, email addresses, URLs, and other fields have the
	 * proper syntax.
	 * 
	 * @param name the name to validate
	 * @return true if valid, false if invalid
	 */

	
	public boolean isValidDate(String date)
	{
		if(date == null || date.isEmpty())
		{
			return true;			
		}
		return isValidDateForNewRecord(date);
		
	}
	
	public boolean isValidDateForNewRecord(String date)
	{
		String strDate ="(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((19|20)\\d\\d)";
		if(date!=null && date.matches(strDate))
		{
			DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd-MM-yyyy");
			dateFormat.parseStrict(date);
			return true;
			
		}
		return false;
	}
	
	public boolean isValidName(String name) {		
	    if( name==null || name.isEmpty())
	    {
	    	return true;
	    }
	    return isValidNameForNewRecord(name);
	}
	
	public boolean isValidNameForNewRecord(String name) {		
	    return name!=null && name.matches("[a-zA-Z]+");
	}
	
	public boolean isValidPhoneNumber(String phoneNumber){
		if(phoneNumber==null || phoneNumber.isEmpty())
		{
			return true;
		}
		return isValidPhoneNumberForNewRecord(phoneNumber);
	}
	
	public boolean isValidPhoneNumberForNewRecord(String phoneNumber){
		return phoneNumber!=null && phoneNumber.matches("[1-9][0-9]{11}");
	}
	
    public boolean checkErrorEntries(int rows, int col, FlexTable table, String type)
    {
       boolean errorFlag = true;

    	for(int row=1; row<=rows; row++)
    	{ 
			TextBox box = (TextBox)table.getWidget(row, col);
    		switch(type)
    		{
	    		case "FIRSTNAME":
	    		{
	    	 		if(!isValidName(box.getText()))
	        		{
	    	 			errorFlag = true;
	    	 			break;
	        		}
	    	 		errorFlag = false;
	    	 		break;
	    		}
	    		case "LASTNAME":
	    		{
	    	 		if(!isValidName(box.getText()))
	        		{
	    	 			errorFlag = true;
	    	 			break;
	        		}
	    	 		errorFlag = false;
	    	 		break;
	    		}
	    		case "DATEOFBIRTH":
	    		{
	    	 		if(!isValidDate(box.getText()))
	        		{
	    	 			errorFlag = true;
	    	 			break;
	        		}
	    	 		errorFlag = false;
	    	 		break;
	    		}   
	    		case "PHONENUMBER":
	    		{
	    	 		if(!isValidDate(box.getText()))
	        		{
	    	 			errorFlag = true;
	    	 			break;
	        		}
	    	 		errorFlag = false;
	    	 		break;
	    		}
    		
    		}
    		if (errorFlag) 
			{
    			break;
			}
    	
       }
    	return errorFlag;
    }
}
