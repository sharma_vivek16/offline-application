package com.customer.records.shared;



import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.customer.records.shared.dto.CustomerDTO;


	@Entity
	@Table(name="customer")
	@Access(value=AccessType.PROPERTY)
	public class Customer implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private int customerId;
		private String firstName;
		private String lastName;
		private String dob;
		private String address;

		private String phoneNumber;

		private String lastSynchronized;

		public Customer() 
		{

		}
		public Customer(CustomerDTO customerDto)
		{
		
			this.firstName = customerDto.getFirstName();
			this.lastName = customerDto.getLastName();
			this.dob = customerDto.getDob();
			this.address = customerDto.getAddress();
			this.phoneNumber = customerDto.getPhoneNumber();
			this.lastSynchronized = customerDto.getLastSynchronized();
		}

		
		public Customer(String firstName, String lastName, String dob, String address, String phoneNumber, String lastSynchronized)
		{
			this.firstName = firstName;
			this.lastName = lastName;
			this.dob = dob;
			this.address = address;
			this.phoneNumber = phoneNumber;
			this.lastSynchronized = lastSynchronized;
		}
		
		public Customer(int customerId, String firstName, String lastName, String dob, String address, String phoneNumber)
		{
			this.customerId = customerId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.dob = dob;
			this.address = address;
			this.phoneNumber = phoneNumber;
			
		}
		
		public String getAddress()
		{
			return address;
		}
		
		@Id
	//	@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "customerid", unique = true, nullable = false)
		public int getCustomerId() 
		{
			return customerId;
		}
		
		@Column(name = "dob")
		public String getDob() 
		{
			return dob;
		}

		@Column(name = "firstname")
		public String getFirstName() 
		{
			return firstName;
		}

		@Column(name = "lastname")
		public String getLastName() 
		{
			return lastName;
		}

		@Column(name = "lastsynchronized")
		public String getLastSynchronized() {
			return lastSynchronized;
		}

		@Column(name = "phonenumber")
		public String getPhoneNumber() {
			return phoneNumber;
		}
		
		@Column(name = "address")
		public void setAddress(String address)
		{
			this.address = address;
		}

		/*
		@OneToOne(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
		public TimeManager getTimeManager() 
		{
			return this.timeManager;
		}
		
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
		public Set<PhoneNumber> getPhoneNumber() {
			return this.phoneNumber;
		}
		
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
		public Set<Address> getAddress() {
			return this.address;
		}
		*/
		public void setCustomerId(int customerId) 
		{
			this.customerId = customerId;
		}
		
		public void setDob(String dob) 
		{
			this.dob = dob;
		}
		
		public void setFirstName(String firstName)
		{
			this.firstName = firstName;
		}

		public void setLastName(String lastName)
		{
			this.lastName = lastName;
		}

		public void setLastSynchronized(String lastSynchronized) 
		{
			this.lastSynchronized = lastSynchronized;
		}
		
		public void setPhoneNumber(String phoneNumber)
		{
			this.phoneNumber = phoneNumber;
		}


	}


