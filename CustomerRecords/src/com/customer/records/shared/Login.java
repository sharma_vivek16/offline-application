package com.customer.records.shared;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="authenticator")
@Access(value=AccessType.PROPERTY)
public class Login implements Serializable
{
	private String username;

	private String password;
	public Login() 
	{
	
	}
	public Login(String username, String password)
	{
		this.username = username;
		this.password = password;
	}
	
	@Column(name = "password")
	public String getPassword() 
	{
		return password;
	}
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "username", unique = true, nullable = false)
	public String getUsername() 
	{
		return username;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public void setUsername(String username) 
	{
		this.username = username;
	}
}
